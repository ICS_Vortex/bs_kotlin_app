package burning.skies.com.service

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.util.Log
import android.support.v4.app.NotificationCompat
import burning.skies.com.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.support.v4.content.ContextCompat
import android.app.NotificationChannel
import android.graphics.Color
import android.os.Build
import org.json.JSONObject

class BurningSkiesFirebaseMessagingService : FirebaseMessagingService() {
    val TAG = "BS_FCM_Service"

    override fun onNewToken(s: String?) {
        Log.e("BS_FCM_NEW_TOKEN", s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        var params = remoteMessage?.data
        var data = JSONObject(params)
        val channelId = "bs_channel"
        Log.d("BS_DEBUG", data.toString())
        val pattern = longArrayOf(0, 1000, 500, 1000)
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(channelId, "BS Notifications",
                    NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.description = ""
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = pattern
            notificationChannel.enableVibration(true)
            mNotificationManager.createNotificationChannel(notificationChannel)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = mNotificationManager.getNotificationChannel(channelId)
            channel.canBypassDnd()
        }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val mVibratePattern = longArrayOf(0, 400, 200, 400)
        notificationBuilder.setAutoCancel(true)
                .setColor(ContextCompat.getColor(this, burning.skies.com.R.color.colorAccent))
                .setContentTitle(remoteMessage?.data?.get("title"))
                .setContentText(remoteMessage?.data?.get("body"))
                .setVibrate(mVibratePattern)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)


        mNotificationManager.notify(1000, notificationBuilder.build())
    }
}