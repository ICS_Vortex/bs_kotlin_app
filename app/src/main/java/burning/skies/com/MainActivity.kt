package burning.skies.com

import android.provider.Settings
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import burning.skies.com.fragment.DogfightsFragment
import burning.skies.com.fragment.ProfileFragment
import burning.skies.com.fragment.ServerFragment
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import android.net.ConnectivityManager
import android.widget.Toast
import org.json.JSONException
import org.json.JSONObject
import okhttp3.*
import java.io.IOException
import android.os.Build
import burning.skies.com.entity.Pilot
import com.google.gson.GsonBuilder
import okhttp3.RequestBody
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import android.view.Menu
import android.view.MenuItem

class MainActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/specialelite.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        setContentView(R.layout.activity_main)

//        setSupportActionBar(toolbar)
        isNetworkAvailable()
        checkGCMClient()
        val appBar = supportActionBar
        val pilotJson = getPilotPreference()
        val gson = GsonBuilder().create()
        val pilot = gson.fromJson(pilotJson, Pilot::class.java)
        appBar?.title = pilot.callsign
        createFragment(ProfileFragment())
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_profile -> {
                createFragment(ProfileFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_server -> {
                createFragment(ServerFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dogfights -> {
                createFragment(DogfightsFragment())
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_logout -> {
                logout()
            }
        }
        false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()

        if (id == R.id.menu_settings) {
            startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            return true
        }

        return super.onOptionsItemSelected(item)

    }
    private fun logout() {
        val sharedPref = getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putInt(getString(R.string.pilot_id), 0)
            apply()
        }
        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        finish()
    }


    private fun createFragment(fragment: Fragment) {
        val manager = supportFragmentManager

        val transaction = manager.beginTransaction()
        transaction.replace(R.id.fragmentHolder, fragment)
        transaction.addToBackStack(null)
        transaction.commit()

    }

    private fun isNetworkAvailable(): Boolean {
        var hasWiFi = false
        var hasMobileData = false

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfos = connectivityManager.allNetworkInfo
        for (info in networkInfos) {
            if (info.typeName.equals("WIFI", ignoreCase = true)) {
                if (info.isConnected) {
                    hasWiFi = true
                }
            }

            if (info.typeName.equals("MOBILE", ignoreCase = true)) {
                if (info.isConnected) {
                    hasMobileData = true
                }
            }
        }

        return hasMobileData || hasWiFi
    }

    private fun checkGCMClient() {
        val token = FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@MainActivity) { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.d("BS_FCM_TOKEN", newToken)
            val oldToken = getGCMToken()
            if (newToken !== oldToken) {
                saveGCMToken(newToken)
                registerGCMToken(newToken)
            }
        }

    }

    private fun registerGCMToken(token: String) {
        val androidIdentifier = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        try {
            val jsonBody = JSONObject()
            jsonBody.put("token", token)
            jsonBody.put("androidIdentifier", androidIdentifier)
            jsonBody.put("androidDeviceName", getDeviceName())
            val mRequestBody = jsonBody.toString()
            val JSON = MediaType.parse("application/json; charset=utf-8")
            val requestBody = RequestBody.create(JSON, mRequestBody)
            val url = getString(R.string.api_host) + getString(R.string.api_mobile_add_client)
            val request = Request.Builder().url(url).post(requestBody).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object: Callback {
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()?.string()
                    try {
                        val res = JSONObject(body)
                        val status = res.getInt("status")
                        val message = res.getString("message")
                        if (status == 1) {
                            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: JSONException) {
                        Log.d("BS_ERROR", "Server messages unavailable!")
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    Log.d("BS_ERROR", e.message)
                    Log.d("BS_ERROR", "Server messages unavailable!")
                }
            })
        } catch (e: Throwable) {
            Log.d("BS_ERROR", "Server messages unavailable!")
        }

    }

    private fun getGCMToken(): String? {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        return sharedPref.getString(getString(R.string.preference_gcm_key), null)
    }

    private fun saveGCMToken(token: String) {
        val sharedPref = getSharedPreferences(getString(R.string.preference_gcm_key), Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putString(getString(R.string.gcm_instance_id), token)
            apply()
        }
    }

    private fun resetGCMToken() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(getString(R.string.preference_gcm_key), "")
        editor.apply()
    }

    private fun getCurrentLocale(): String {
        val lang: String
        val currentLocale = resources.configuration.locale.language
        lang = when (currentLocale) {
            "uk" -> "ua"
            "ru" -> "ru"
            "en" -> "en"
            else -> "en"
        }

        return lang
    }

    private fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            model
        } else {
            "$manufacturer $model"
        }
    }

    private fun getPilotPreference(): String? {
        val sharedPref = getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
        val defaultValue = "{}"
        return sharedPref.getString(getString(R.string.pilot_json), defaultValue)
    }
}
