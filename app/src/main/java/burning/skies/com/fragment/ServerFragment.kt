package burning.skies.com.fragment

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import burning.skies.com.R
import burning.skies.com.entity.ServerStatus
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_server.*
import okhttp3.*
import java.io.IOException

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ServerFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val url = getString(R.string.api_host) + getString(R.string.api_server_status)
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        val mHandler = Handler(Looper.getMainLooper())

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                val gson = GsonBuilder().create()
                val serverData = gson.fromJson(body, ServerStatus::class.java)
                val runnable = Runnable {
                    if(serverData.isOnline) {
                        serverStatus.text = getString(R.string.status_online)
                        serverStatus.setTextColor(Color.GREEN)
                    } else {
                        serverStatus.text = getString(R.string.status_offline)
                        serverStatus.setTextColor(Color.DKGRAY)
                    }
                    missionName.text = serverData.mission.substring(0, 16) + "..."
                    startTime.text = serverData.startedAgo.hour.toString() + ":" + serverData.startedAgo.minute.toString() + " h. ago"
                    redTeam.text = serverData.online.red.count().toString()
                    blueTeam.text = serverData.online.blue.count().toString()
                    spectatorsTeam.text = serverData.online.spectator.count().toString()
                }
                mHandler.post {
                    runnable.run()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_server, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ServerFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
