package burning.skies.com.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import burning.skies.com.MainActivity
import burning.skies.com.R
import burning.skies.com.entity.Pilot
import burning.skies.com.entity.PilotInfo
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_server.*
import okhttp3.*
import java.io.IOException
import java.net.URLEncoder
import kotlin.system.exitProcess

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ProfileFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val gson = GsonBuilder().create()
        val pilotJson = getPilotPreference()
        val pilot = gson.fromJson(pilotJson, Pilot::class.java)
        val mHandler = Handler(Looper.getMainLooper())
        val client = OkHttpClient()
        val url = getString(R.string.api_host) + getString(R.string.api_pilot_extended_info) + pilot.id.toString()
        val request = Request.Builder().url(url).build()

        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                val gson = GsonBuilder().create()
                val info = gson.fromJson(body, PilotInfo::class.java)

                val runnable = Runnable {
                    tvCallsign.text = pilot.callsign
                    airkills.text = info.airkills.toString()
                    groundKills.text = info.groundKills.toString()
                    groundScore.text = info.groundPoints.toString()
                    flightHours.text = info.hours
                    sorties.text = info.sorties.toString()
                    if (info.sorties < info.landings) {
                        landings.text = info.sorties.toString()
                    } else {
                        landings.text = info.landings.toString()
                    }
                }
                mHandler.post {
                    runnable.run()
                }
            }

            override fun onFailure(call: Call, e: IOException) {

            }
        })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ProfileFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private fun getPilotPreference(): String? {
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
        val defaultValue = "{}"
        return sharedPref.getString(getString(R.string.pilot_json), defaultValue)
    }
}
