package burning.skies.com.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar

import burning.skies.com.R
import burning.skies.com.adapter.DogfightAdapter
import burning.skies.com.entity.Dogfight
import burning.skies.com.entity.Pilot
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DogfightsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dogfights, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val pb = view?.findViewById(R.id.dogfightsProgressBar) as ProgressBar?
        pb?.visibility = View.VISIBLE
        val dogfightsRecycler = view?.findViewById(R.id.dogfightsRecycler) as RecyclerView
        dogfightsRecycler.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        val url = getString(R.string.api_host) + getString(R.string.api_pilots_dogfights) + getPilot().id
        Log.d("BS_URL", url)
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        val mHandler = Handler(Looper.getMainLooper())
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                val gson = GsonBuilder().create()
                val dogfights: List<Dogfight> = gson.fromJson(body, Array<Dogfight>::class.java).toList()
                val dogfightsAdapter = DogfightAdapter(dogfights, context)
                val runnable = Runnable {
                    pb?.visibility = View.GONE
                    dogfightsRecycler.adapter = dogfightsAdapter
                }
                mHandler.post {
                    runnable.run()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute request")
            }
        })

    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DogfightsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private fun getPilot() :Pilot {
        val sharedPref = context?.getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
        val defaultValue : String = "{}"
        val json : String? = sharedPref?.getString(getString(R.string.pilot_json), defaultValue)

        val gson = GsonBuilder().create()
        return gson.fromJson(json, Pilot::class.java)
    }
}
