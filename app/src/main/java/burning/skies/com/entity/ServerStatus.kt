package burning.skies.com.entity

import com.google.gson.annotations.SerializedName

data class ServerStatus(
    @SerializedName("status") var status: Int,
    @SerializedName("isOnline") var isOnline: Boolean,
    @SerializedName("mission") var mission: String,
    @SerializedName("started_ago") var startedAgo: StartedAgo,
    @SerializedName("online") var online: Online
) {
    data class Online(
        @SerializedName("BLUE") var blue: List<Blue>,
        @SerializedName("RED") var red: List<Red>,
        @SerializedName("SPECTATOR") var spectator: List<Spectator>
    ) {
        data class Red(
            @SerializedName("id") var id: Int,
            @SerializedName("callsign") var callsign: String,
            @SerializedName("plane") var plane: String,
            @SerializedName("image") var image: String,
            @SerializedName("entered") var entered: String
        )

        data class Blue(
            @SerializedName("id") var id: Int,
            @SerializedName("callsign") var callsign: String,
            @SerializedName("plane") var plane: String,
            @SerializedName("image") var image: String,
            @SerializedName("entered") var entered: String
        )

        data class Spectator(
            @SerializedName("id") var id: Int,
            @SerializedName("callsign") var callsign: String,
            @SerializedName("plane") var plane: String,
            @SerializedName("image") var image: String,
            @SerializedName("entered") var entered: String
        )
    }

    data class StartedAgo(
        @SerializedName("hour") var hour: Int,
        @SerializedName("minute") var minute: Int,
        @SerializedName("second") var second: Int
    )
}