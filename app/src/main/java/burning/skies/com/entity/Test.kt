package burning.skies.com.entity

import com.google.gson.annotations.SerializedName


data class Test(
        @SerializedName("BLUE") val bLUE: List<BLUE?>?,
        @SerializedName("RED") val rED: List<RED?>?,
        @SerializedName("SPECTATOR") val sPECTATOR: List<SPECTATOR?>?
) {

    data class RED(
            @SerializedName("id") val id: Int?,
            @SerializedName("callsign") val callsign: String?,
            @SerializedName("side") val side: String?,
            @SerializedName("plane") val plane: String?,
            @SerializedName("image") val image: String?,
            @SerializedName("entered") val entered: String?
    )


    data class SPECTATOR(
            @SerializedName("id") val id: Int?,
            @SerializedName("callsign") val callsign: String?,
            @SerializedName("side") val side: String?,
            @SerializedName("plane") val plane: Any?,
            @SerializedName("image") val image: Any?,
            @SerializedName("entered") val entered: String?
    )


    data class BLUE(
            @SerializedName("id") val id: Int?,
            @SerializedName("callsign") val callsign: String?,
            @SerializedName("side") val side: String?,
            @SerializedName("plane") val plane: String?,
            @SerializedName("image") val image: String?,
            @SerializedName("entered") val entered: String?
    )
}