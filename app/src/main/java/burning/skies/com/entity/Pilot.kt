package burning.skies.com.entity

import com.google.gson.annotations.SerializedName

data class Pilot(
        @SerializedName("id") var id: Int,
        @SerializedName("callsign") var callsign: String,
        @SerializedName("isActive") var isActive: Boolean,
        @SerializedName("ipAddress") var ipAddress: String,
        @SerializedName("country") var country: String,
        @SerializedName("location") var location: String
)