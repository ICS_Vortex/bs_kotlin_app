package burning.skies.com.entity

import com.google.gson.annotations.SerializedName


data class Dogfight(
        @SerializedName("id") val id: Int,
        @SerializedName("pilot") val pilot: Pilot,
        @SerializedName("plane") val plane: Plane,
        @SerializedName("side") val side: String,
        @SerializedName("victim") val victim: Victim,
        @SerializedName("victimPlane") val victimPlane: VictimPlane,
        @SerializedName("victimSide") val victimSide: String,
        @SerializedName("killTime") val killTime: String,
        @SerializedName("friendly") val friendly: Boolean,
        @SerializedName("points") val points: Int
) {

    data class Plane(
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String,
            @SerializedName("image") val image: Any
    )


    data class Pilot(
            @SerializedName("id") val id: Int,
            @SerializedName("callsign") val callsign: String,
            @SerializedName("isActive") val isActive: Boolean,
            @SerializedName("ipAddress") val ipAddress: String,
            @SerializedName("country") val country: String,
            @SerializedName("location") val location: String
    )


    data class VictimPlane(
            @SerializedName("id") val id: Int,
            @SerializedName("name") val name: String,
            @SerializedName("image") val image: Any
    )


    data class Victim(
            @SerializedName("id") val id: Int,
            @SerializedName("callsign") val callsign: String,
            @SerializedName("isActive") val isActive: Boolean,
            @SerializedName("ipAddress") val ipAddress: String,
            @SerializedName("country") val country: String,
            @SerializedName("location") val location: String
    )
}