package burning.skies.com.entity

import com.google.gson.annotations.SerializedName

data class PilotInfo(
        @SerializedName("nickname") var callsign: String,
        @SerializedName("ground_kills") var groundKills: Int,
        @SerializedName("ground_points") var groundPoints: Int,
        @SerializedName("airkills") var airkills: Int,
        @SerializedName("temp_streak") var temporaryStreak: Int,
        @SerializedName("best_streak") var bestStreak: Int,
        @SerializedName("hours") var hours: String,
        @SerializedName("sorties") var sorties: Int,
        @SerializedName("landings") var landings: Int
)