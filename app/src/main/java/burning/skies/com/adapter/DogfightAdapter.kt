package burning.skies.com.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import burning.skies.com.R
import burning.skies.com.entity.Dogfight
import com.squareup.picasso.Picasso

class DogfightAdapter(private val dogfights : List<Dogfight>,private val context: Context?) : RecyclerView.Adapter<DogfightAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DogfightAdapter.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.dogfight_layout, p0,false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return dogfights.size
    }

    override fun onBindViewHolder(p0: DogfightAdapter.ViewHolder, p1: Int) {
        val dogfight: Dogfight = dogfights[p1]
        p0.pilotCallsign.text = dogfight.pilot.callsign
//        p0.pilotPlane. = dogfight.plane.name
        p0.victimCallsign.text = dogfight.victim.callsign
//        p0.victimPlane.hint = dogfight.victimPlane.name
        if (dogfight.side == "RED") {
            p0.pilotCallsign.setTextColor(Color.RED)
            p0.pilotCallsign.setTextColor(Color.RED)
        } else {
            p0.pilotCallsign.setTextColor(Color.BLUE)
            p0.pilotCallsign.setTextColor(Color.BLUE)
        }
        if (dogfight.victimSide == "RED") {
            p0.victimCallsign.setTextColor(Color.RED)
            p0.victimCallsign.setTextColor(Color.RED)
        } else {
            p0.victimCallsign.setTextColor(Color.BLUE)
            p0.victimCallsign.setTextColor(Color.BLUE)
        }
        val imagesPath = context?.getString(R.string.api_host) + context?.getString(R.string.images_path)
        val pilotPlaneUrl = imagesPath + dogfight.plane.name.toLowerCase() + context?.getString(R.string.extension_png)
        val victimPlaneUrl = imagesPath + dogfight.victimPlane.name.toLowerCase() + context?.getString(R.string.extension_png)
        Log.d("BS_IMAGE", pilotPlaneUrl)
        Log.d("BS_IMAGE", victimPlaneUrl)
        Picasso.get().load(pilotPlaneUrl).error(R.drawable.ic_question).into(p0.pilotPlane)
        Picasso.get().load(victimPlaneUrl).error(R.drawable.ic_question).into(p0.victimPlane)
        p0.victimPlane.scaleX = -1F
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pilotCallsign = itemView.findViewById(R.id.pilotCallsign) as TextView
        val pilotPlane = itemView.findViewById(R.id.pilotPlane) as ImageView
        val victimCallsign = itemView.findViewById(R.id.victimCallsign) as TextView
        val victimPlane = itemView.findViewById(R.id.victimPlane) as ImageView
    }
}