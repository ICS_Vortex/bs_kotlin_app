package burning.skies.com

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.EditText
import android.widget.Toast
import burning.skies.com.entity.Pilot
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException
import java.net.URLEncoder
import kotlin.system.exitProcess
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class LoginActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/specialelite.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
        if(checkPreferences() != 0) {
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            finish()
            return
        }

        setContentView(R.layout.activity_login)
        val editTextCallsign = findViewById<EditText>(R.id.editTextCallsign)
        val btnLogin = findViewById<View>(R.id.btnLogin)
        btnLogin.setOnClickListener(View.OnClickListener {
            if(editTextCallsign.text.isEmpty()) {
                Toast.makeText(this@LoginActivity, getString(R.string.text_invalid_callsign), Toast.LENGTH_LONG).show()
                return@OnClickListener
            }
            val url = getString(R.string.api_host) + getString(R.string.api_get_pilot) + URLEncoder.encode(editTextCallsign.text.toString())
            val request = Request.Builder().url(url).build()
            val client = OkHttpClient()
            val mHandler = Handler(Looper.getMainLooper())
            client.newCall(request).enqueue(object: Callback {
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()?.string()
                    if (!body.equals("[]")) {
                        val gson = GsonBuilder().create()
                        val pilot = gson.fromJson(body, Pilot::class.java)
                        val sharedPref = getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
                        with(sharedPref.edit()) {
                            putInt(getString(R.string.pilot_id), pilot.id)
                            putString(getString(R.string.pilot_json), body)
                            apply()
                        }
                        val runnable = Runnable {
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            finish()
                        }
                        mHandler.post {
                            runnable.run()
                        }
                        return
                    }

                    val builder = AlertDialog.Builder(this@LoginActivity)
                    builder.setTitle(getString(R.string.app_name))
                    builder.setMessage("Pilot " + editTextCallsign.text.toString() +" not found.")
                    builder.setPositiveButton(getString(R.string.ok)){_, _ ->
                    }

                    builder.setNegativeButton(getString(R.string.title_exit)){ _, _ ->
                        moveTaskToBack(true)
                        exitProcess(-1)
                    }


                    val runnable = Runnable {
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
                    mHandler.post {
                        runnable.run()
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    val builder = AlertDialog.Builder(this@LoginActivity)
                    builder.setTitle(getString(R.string.app_name))
                    builder.setMessage(R.string.unable_to_connect)

                    builder.setNegativeButton(getString(R.string.title_exit)){ _, _ ->
                        moveTaskToBack(true)
                        exitProcess(-1)
                    }

                    val runnable = Runnable {
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }

                    mHandler.post {
                        runnable.run()
                    }
                }
            })
        })
    }

    private fun checkPreferences(): Int {
        val sharedPref = getSharedPreferences(getString(R.string.preference_pilot), Context.MODE_PRIVATE)
        val defaultValue = 0
        return sharedPref.getInt(getString(R.string.pilot_id), defaultValue)
    }
}
